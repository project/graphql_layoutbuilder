<?php

namespace Drupal\graphql_layoutbuilder\Plugin\Deriver;

use Drupal\Core\Layout\LayoutDefinition;
use Drupal\graphql\Utility\StringHelper;

/**
 * Class SectionPropertyDeriver
 *
 * Deriver for section properties.
 *
 * @package Drupal\graphql_layoutbuilder\Plugin\Deriver
 */
class SectionPropertyDeriver extends SectionTypeDeriver {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    foreach ($this->layoutPluginManager->getDefinitions() as $sectionType => $definition) {
      if ($definition instanceof LayoutDefinition) {
        $properties = [
          'category',
          'label',
          'description',
          'regions',
        ];

        foreach ($properties as $propertyName) {
          $property = $definition->get($propertyName);
          $type = is_array($property) ? '[String]' : 'String';
          $derivative = [
            'parents' => ['Section'],
            'name' => StringHelper::propCase($propertyName),
            'type' => $type,
            'propertyName' => $propertyName,
          ] + $basePluginDefinition;
          $this->derivatives["{$sectionType}-$propertyName"] = $derivative;
        }

      }
    }
    return parent::getDerivativeDefinitions($basePluginDefinition);
  }

}
