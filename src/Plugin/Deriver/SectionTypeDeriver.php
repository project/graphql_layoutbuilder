<?php

namespace Drupal\graphql_layoutbuilder\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\graphql\Utility\StringHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SectionTypeDeriver
 *
 * Deriver for section types.
 *
 * @package Drupal\graphql_layoutbuilder\Plugin\Deriver
 */
class SectionTypeDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager, LayoutPluginManagerInterface $layout_plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->layoutPluginManager = $layout_plugin_manager;
  }

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.core.layout')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    foreach ($this->layoutPluginManager->getDefinitions() as $sectionType => $definition) {
      $type = 'layout_' . $sectionType;
      $derivative = [
        'name' => StringHelper::camelCase($type),
        'interfaces' => ['Section'],
        'type' => $type,
      ] + $basePluginDefinition;
      $this->derivatives[$type] = $derivative;
    }
    return parent::getDerivativeDefinitions($basePluginDefinition);
  }

}
