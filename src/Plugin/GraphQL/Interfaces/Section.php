<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Interfaces;

use Drupal\graphql\Plugin\GraphQL\Interfaces\InterfacePluginBase;

/**
 * @GraphQLInterface(
 *   id = "section",
 *   name = "Section",
 *   type = "layoutbuilder_section",
 *   description = @Translation("Layout interface containing layout sections.")
 * )
 */
class Section extends InterfacePluginBase {

}
