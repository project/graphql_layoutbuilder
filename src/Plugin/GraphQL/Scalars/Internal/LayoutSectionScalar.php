<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Scalars\Internal;

use Drupal\graphql\Plugin\GraphQL\Scalars\Internal\StringScalar;

/**
 * Layout Builder module defines a custom data type that essentially is a
 * string, but not called string.
 *
 * @GraphQLScalar(
 *   id = "layout_section",
 *   name = "layout_section"
 * )
 */
class LayoutSectionScalar extends StringScalar {
}
