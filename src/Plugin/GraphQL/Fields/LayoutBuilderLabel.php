<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\SectionStorageInterface;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * GraphQL does not allow types without a field.
 *
 * So the first field of the LayoutType is section_type.
 *
 * @GraphQLField(
 *   id = "layout_builder_label",
 *   secure = true,
 *   name = "label",
 *   type = "String",
 *   parents = {"LayoutBuilder"},
 * )
 */
class LayoutBuilderLabel extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof SectionStorageInterface) {
      yield $value->label();
    }
  }

}
