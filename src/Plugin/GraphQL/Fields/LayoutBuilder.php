<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\Context\EntityContext;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\layout_builder\LayoutTempstoreRepositoryInterface;
use Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface;

/**
 * Layout Builder context field.
 *
 * @GraphQLField(
 *   id = "layout_builder",
 *   secure = true,
 *   name = "layoutBuilder",
 *   type = "LayoutBuilder",
 *   parents = {"Entity"},
 * )
 */
class LayoutBuilder extends FieldPluginBase implements ContainerFactoryPluginInterface {

  use LayoutBuilderContextTrait;

  /**
   * The section storage manager.
   *
   * @var \Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface
   */
  protected $sectionStorageManager;

  /**
   * The Layout Tempstore.
   *
   * @var \Drupal\layout_builder\LayoutTempstoreRepositoryInterface
   */
  protected $layoutTempstore;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('plugin.manager.layout_builder.section_storage'),
      $container->get('layout_builder.tempstore_repository')
    );
  }

  /**
   * BlocksByRegion constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\layout_builder\SectionStorage\SectionStorageManagerInterface $section_storage_manager
   *   The storage manager.
   * @param \Drupal\layout_builder\LayoutTempstoreRepositoryInterface $layout_tempstore_repository
   *   The layout tempstore.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    SectionStorageManagerInterface $section_storage_manager,
    LayoutTempstoreRepositoryInterface $layout_tempstore_repository
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->sectionStorageManager = $section_storage_manager;
    $this->layoutTempstore = $layout_tempstore_repository;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof EntityInterface) {
      $node_context = EntityContext::fromEntity($value);
      $context->setContext('entity', $value, $info);
      $section_storage = $this->sectionStorageManager->load('overrides', ['entity' => $node_context]);
      $layout_builder_context = $this->getPopulatedContexts($section_storage);
      $context->setContext('layout_builder', $layout_builder_context, $info);
      yield $section_storage;
    }
    else {
      yield NULL;
    }

  }

}
