<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Sections;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\Section;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * GraphQL does not allow types without a field.
 *
 * So the first field of the LayoutType is section_type.
 *
 * @GraphQLField(
 *   id = "section_type",
 *   secure = true,
 *   name = "sectionType",
 *   type = "String",
 *   parents = {"Section"},
 * )
 */
class SectionTypeField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof Section) {
      yield $value->getLayoutId();
    }
  }

}
