<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Sections;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\Section;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @GraphQLField(
 *   secure = true,
 *   id = "section_property",
 *   secure = true,
 *   deriver = "Drupal\graphql_layoutbuilder\Plugin\Deriver\SectionPropertyDeriver",
 * )
 */
class SectionProperty extends FieldPluginBase implements ContainerFactoryPluginInterface {

  use DependencySerializationTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof Section) {
      $result = $value->getLayout()->getPluginDefinition()->get($this->getPluginDefinition()['propertyName']);
      if (is_array($result)) {
        foreach ($result as $key => $item) {
          yield $key;
        }
      }
      else {
        yield $result;
      }
    }

  }

}
