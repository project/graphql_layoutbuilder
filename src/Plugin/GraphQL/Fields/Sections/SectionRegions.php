<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Sections;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\Section;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "section_region",
 *   secure = true,
 *   name = "regions",
 *   type = "[String]",
 *   parents = {"Section"},
 * )
 */
class SectionRegions extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof Section) {
      $regions = $value->getLayout()->getPluginDefinition()->getRegions();
      foreach ($regions as $region_key => $region) {
        yield $region_key;
      }
    }
  }

}
