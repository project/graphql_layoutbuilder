<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Sections;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\Section;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "section_delta",
 *   secure = true,
 *   name = "delta",
 *   type = "Int",
 *   parents = {"Section"},
 * )
 */
class SectionDelta extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof Section) {
      yield $value->getThirdPartySetting('graphql', 'delta');
    }
  }

}
