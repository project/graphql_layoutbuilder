<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Sections;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\OverridesSectionStorageInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\node\Entity\Node;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * GraphQL does not allow types without a field.
 *
 * So the first field of the LayoutType is section_type.
 *
 * @GraphQLField(
 *   id = "layout_reference",
 *   secure = true,
 *   name = "sections",
 *   type = "[Section]",
 *   parents = {"SectionStorage"},
 * )
 */
class Sections extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof SectionStorageInterface) {
      $sections = $value->getSections();

      /** @var \Drupal\layout_builder\Section $section */
      foreach ($sections as $key => $section) {
        // The section does not have a unique id in the database. So we have to generate one.
        // Format = NodeID + LayoutId + OrderID (weight) of section
        $section_id = implode(
          '_',
          [
            $value->getStorageType(),
            $value->getStorageId(),
            $section->getLayoutId(),
            $key,
          ]
        );
        $section->setThirdPartySetting('graphql', 'key', $section_id);
        $section->setThirdPartySetting('graphql', 'delta', $key);
        yield $section;
      }
    }
    else {
      yield NULL;
    }

  }

}
