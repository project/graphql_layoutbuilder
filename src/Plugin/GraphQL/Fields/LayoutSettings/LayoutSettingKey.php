<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\LayoutSettings;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "layout_setting_key",
 *   secure = true,
 *   name = "key",
 *   type = "String",
 *   parents = {"LayoutSetting"},
 * )
 */
class LayoutSettingKey extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if (is_array($value)) {
      yield key($value);
    }
  }

}
