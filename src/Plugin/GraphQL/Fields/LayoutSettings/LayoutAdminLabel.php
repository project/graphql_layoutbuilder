<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\LayoutSettings;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\Section;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "layout_admin_label",
 *   secure = true,
 *   name = "adminLabel",
 *   type = "String",
 *   parents = {"Section"}
 * )
 */
class LayoutAdminLabel extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof Section) {
      if (!empty($layout_settings = $value->getLayoutSettings()) && isset($layout_settings['label'])) {
        yield $layout_settings['label'];
      }
    }
  }

}
