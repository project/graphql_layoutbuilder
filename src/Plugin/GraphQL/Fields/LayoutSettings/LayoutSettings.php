<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\LayoutSettings;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\Section;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "layout_settings",
 *   secure = true,
 *   name = "layoutSettings",
 *   type = "[LayoutSetting]",
 *   parents = {"Section"},
 * )
 */
class LayoutSettings extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof Section) {
      $layout_settings = $value->getLayoutSettings();

      $black_list = ['label'];
      foreach ($black_list as $key) {
        unset($layout_settings[$key]);
      }

      foreach ($layout_settings as $setting_key => $value) {
        if (is_array($value)) {
          foreach ($value as $subvalue) {
            yield [$setting_key => $subvalue];
          }
        }
        else {
          yield [$setting_key => $value];
        }
      }
    }
  }

}
