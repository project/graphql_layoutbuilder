<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * GraphQL does not allow types without a field.
 *
 * So the first field of the LayoutType is section_type.
 *
 * @GraphQLField(
 *   id = "section_storage",
 *   secure = true,
 *   name = "sectionStorage",
 *   type = "SectionStorage",
 *   parents = {"LayoutBuilder"},
 * )
 */
class SectionStorage extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof SectionStorageInterface) {
      $context->setContext('section_storage', $value, $info);
      yield $value;
    }
    else {
      yield NULL;
    }
  }

}
