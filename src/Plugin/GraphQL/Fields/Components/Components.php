<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Components;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * List all components of a section
 *
 * @GraphQLField(
 *   id = "section_components",
 *   secure = true,
 *   name = "components",
 *   type = "[Component]",
 *   parents = {"Section"},
 * )
 */
class Components extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof Section) {
      $components = $value->getComponents();

      usort($components, function (SectionComponent $a, SectionComponent $b) {
        return $a->getWeight() <=> $b->getWeight();
      });

      /** @var \Drupal\layout_builder\SectionComponent $component */
      foreach ($components as $component) {
        yield $component;
      }
    };
  }

}
