<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Components;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\SectionComponent;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "component_label",
 *   secure = true,
 *   name = "label",
 *   type = "String",
 *   parents = {"Component"}
 * )
 */
class ComponentLabel extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof SectionComponent) {
      if (!empty($configuration = $value->get('configuration')) && isset($configuration['label'])) {
        yield $configuration['label'];
      }
    }
  }

}
