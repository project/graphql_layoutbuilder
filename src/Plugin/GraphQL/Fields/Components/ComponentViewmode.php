<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Components;

use Drupal\Core\Entity\EntityInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "component_viewmode",
 *   secure = true,
 *   name = "viewmode",
 *   type = "String",
 *   parents = {"Entity"}
 * )
 */
class ComponentViewmode extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof EntityInterface) {

      $block = $context->getContext('block', $info);

      if ($block && !empty($configuration = $block->getConfiguration())) {

        // Case: Block Content.
        if (isset($configuration['view_mode'])) {
          yield $configuration['view_mode'];
        }
      }

    }
  }

}
