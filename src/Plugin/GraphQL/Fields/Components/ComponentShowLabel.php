<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Components;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\SectionComponent;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "component_show_label",
 *   secure = true,
 *   name = "showLabel",
 *   type = "Boolean",
 *   parents = {"Component"}
 * )
 */
class ComponentShowLabel extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof SectionComponent) {
      if (!empty($configuration = $value->get('configuration')) && isset($configuration['label_display'])) {
        yield $configuration['label_display'];
      }
    }
  }

}
