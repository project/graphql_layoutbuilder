<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Components;

use Drupal\block_content\Plugin\Block\BlockContentBlock;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Cache\CacheableValue;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\graphql\Plugin\TypePluginManagerInterface;
use Drupal\layout_builder\Event\SectionComponentBuildRenderArrayEvent;
use Drupal\layout_builder\LayoutBuilderEvents;
use Drupal\layout_builder\Plugin\Block\FieldBlock;
use Drupal\layout_builder\Plugin\Block\InlineBlock;
use Drupal\layout_builder\SectionComponent;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * List all blocks of a component.
 *
 * @GraphQLField(
 *   id = "component_blocks",
 *   secure = true,
 *   name = "content",
 *   type = "Entity",
 *   parents = {"Component"},
 * )
 */
class ComponentBlocks extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\graphql\Plugin\TypePluginManagerInterface
   */
  protected $typeManager;

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.graphql.type'),
      $container->get('entity.repository'),
      $container->get('event_dispatcher'),
      $container->get('module_handler')
    );
  }

  /**
   * BlocksByRegion constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    TypePluginManagerInterface $manager,
    EntityRepositoryInterface $entityRepository,
    $event_dispatcher,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entityTypeManager;
    $this->typeManager = $manager;
    $this->entityRepository = $entityRepository;
    $this->eventDispatcher = $event_dispatcher;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  protected function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $language = $context->getContext('language', $info);

    if ($value instanceof SectionComponent) {
      $plugin = $value->getPlugin();

      if ($this->moduleHandler->moduleExists('layout_builder_st')) {
        $section_storage = $context->getContext('section_storage', $info);
        $uuid = $value->getUuid();
        $translated_configuration = $section_storage->getTranslatedComponentConfiguration($uuid);
        $translated_configuration += $plugin->getConfiguration();
        $plugin->setConfiguration($translated_configuration);
      }

      // Set the block plugin as a context.
      $context->setContext('block', $plugin, $info);

      // Reusable block from block library.
      if ($plugin instanceof BlockContentBlock) {
        /** @var \Drupal\block_content\BlockContentInterface $block */
        $block = $this->entityRepository->loadEntityByUuid('block_content', $plugin->getDerivativeId());
        if ($block->isTranslatable() && $block->hasTranslation($language)) {
          $block = $block->getTranslation($language);
        }
        yield new CacheableValue($block, [$value]);
      }

      // LayoutBuilder inline block (not reusable).
      if ($plugin instanceof InlineBlock) {
        // InlineBlocks in a temporary SectionStorage only exist as a
        // serialized string. We have to build the render array for the
        // inline block and then access the BlockContent entity of that.
        $build = $plugin->build();
        $block_configuration = $plugin->getConfiguration();
        $build = $plugin->build();
        if ($block_configuration['block_serialized']) {
          yield $build['#block_content'];
        }
        else {
          $revision_id = $block_configuration['block_revision_id'];
          /** @var \Drupal\block_content\BlockContentInterface $block */
          $block = $this->entityTypeManager->getStorage('block_content')->loadRevision($revision_id);

          if ($block) {
            if ($block->isTranslatable() && $block->hasTranslation($language)) {
              $block = $block->getTranslation($language);
            }

            yield new CacheableValue($block, [$value]);
          }
        }
      }

      // Entity provided by entity_block module.
      // if ($plugin instanceof EntityBrowserBlock) {
      //   // Set entities as a context.
      //   $entity = NULL;
      //   $configuration = $plugin->getConfiguration();
      //   if ($entity_ids = $configuration['entity_ids']) {
      //     // EntityBrowser is configured to allow only one entity.
      //     $entities = $this->loadEntities($entity_ids);
      //     $entity = reset($entities);
      //   }
      //   $context->setContext('entity_browser_entity', $entity, $info);
      //
      //   yield new CacheableValue($plugin, [$value]);
      // }

      // FieldBlock by Layoutbuilder.
      // It is too complicated to expose a single field. For now, just expose the parent node.
      if ($plugin instanceof FieldBlock) {
        // $entity = $context->getContext('entity', $info, NULL);
        // if ($entity) {
        //   yield new CacheableValue($entity, [$value]);
        // }
      }

      if ($plugin instanceof BlockBase) {
        yield new CacheableValue($plugin, [$value]);
      }
    }
  }

  /**
   * Loads entities based on an ID in the format entity_type:entity_id.
   *
   * @param array $ids
   *   An array of IDs.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of loaded entities, keyed by an ID.
   */
  public function loadEntities($ids) {
    $storages = [];
    $entities = [];
    foreach ($ids as $id) {
      list($entity_type_id, $entity_id) = explode(':', $id);
      if (!isset($storages[$entity_type_id])) {
        $storages[$entity_type_id] = $this->entityTypeManager->getStorage($entity_type_id);
      }
      $entities[$entity_type_id . ':' . $entity_id] = $storages[$entity_type_id]->load($entity_id);
    }
    return $entities;
  }

}
