<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Fields\Components;

use Drupal\block_content\BlockContentUuidLookup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\SectionComponent;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin ID of a component.
 *
 * @GraphQLField(
 *   id = "component_plugin_id",
 *   secure = true,
 *   name = "pluginId",
 *   type = "String",
 *   parents = {"Component"}
 * )
 */
class ComponentPluginId extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The block content UUID lookup service.
   *
   * @var \Drupal\block_content\BlockContentUuidLookup
   */
  protected $uuidLookup;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition
  ) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('block_content.uuid_lookup')
    );
  }

  /**
   * BlocksByRegion constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\block_content\BlockContentUuidLookup $uuid_lookup
   *   The block UUID lookup.
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entity_type_manager,
    BlockContentUuidLookup $uuid_lookup
  ) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->entityTypeManager = $entity_type_manager;
    $this->uuidLookup = $uuid_lookup;
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $id = '';
    if ($value instanceof SectionComponent) {
      $configuration = $value->get('configuration') ?? [];
      $provider = $configuration['provider'] ?? '';
      $type = $configuration['type'] ?? '';

      if (!$provider) {
        $id = $configuration['id'] ?? '';
        $parts = explode(':', $id);
        $uuid = end($parts);
        if ($uuid) {
          $block_id = $this->uuidLookup->get($uuid);
          if ($block_id) {
            /** @var \Drupal\block_content\BlockContentInterface $block */
            $block = $this->entityTypeManager->getStorage('block_content')->load($block_id);
            if ($block) {
              $type = $block->bundle();
              $provider = 'block_content';
            }
          }
        }
      }

      // Because reusable blockss are block_content:UUID, but we need the
      // "inline_block" type format.
      if ($provider === 'block_content' && $type) {
        $id = 'inline_block:' . $type;
      } else {
        $id = $configuration['id'];
      }
    }

    if ($id) {
      yield $id;
    }
  }

}
