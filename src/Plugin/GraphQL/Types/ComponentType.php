<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * @GraphQLType(
 *   id = "component_type",
 *   name = "Component",
 * )
 */
class ComponentType extends TypePluginBase {


}
