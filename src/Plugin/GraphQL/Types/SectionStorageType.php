<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Types;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;
use Drupal\layout_builder\SectionStorageInterface;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * @GraphQLType(
 *   id = "section_storage_type",
 *   name = "SectionStorage",
 * )
 */
class SectionStorageType extends TypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function applies($object, ResolveContext $context, ResolveInfo $info) {
    return $object instanceof SectionStorageInterface;
  }

}
