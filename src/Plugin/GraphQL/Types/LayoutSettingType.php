<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * @GraphQLType(
 *   id = "layout_setting_type",
 *   name = "LayoutSetting",
 * )
 */
class LayoutSettingType extends TypePluginBase {


}
