<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Types;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;
use Drupal\layout_builder\Section;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * @GraphQLType(
 *   id = "layout_type",
 *   deriver = "Drupal\graphql_layoutbuilder\Plugin\Deriver\SectionTypeDeriver",
 * )
 */
class SectionType extends TypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function applies($object, ResolveContext $context, ResolveInfo $info) {
    if ($object instanceof Section) {
      $definition = $this->getPluginDefinition();

      if (!empty($definition) && isset($definition['type'])) {
        $type = $definition['type'];
        $section_type = 'layout_' . $object->getLayoutId();
        return $type === $section_type;
      }
    }

    return FALSE;
  }

}
