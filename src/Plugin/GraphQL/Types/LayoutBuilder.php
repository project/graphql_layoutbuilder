<?php

namespace Drupal\graphql_layoutbuilder\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * @GraphQLType(
 *   id = "layout_builder_type",
 *   name = "LayoutBuilder",
 * )
 */
class LayoutBuilder extends TypePluginBase {

}
