INTRODUCTION
------------

The GraphQL Layout Builder module allows you to build decoupled websites using GraphQL and the full power of layout builder.
On any page with layout builder enabled you will find a new GraphQL field called "sections" containing "components".
Inside the components field you will find "contents" like blocks and their corresponding fields.

* For a full description of the module visit:
  https://www.drupal.org/project/graphql_layoutbuilder

* To submit bug reports and feature suggestions, or to track changes visit:
  https://www.drupal.org/project/issues/graphql_layoutbuilder


REQUIREMENTS
------------

This module requires [GraphQL Module 3](https://www.drupal.org/project/graphql) and the Layout Builder module from Drupal Core.


INSTALLATION
------------

* Install the GraphQL Layout Builder module as you would normally install a
  contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
  further information.


CONFIGURATION
-------------

There is no real configuration necessary. Navigate to Administration >
Extend and enable the module.

Any entity with Layout Builder enabled will expose its field to GraphQL automatically.

There is also support for the [Layout Options](https://www.drupal.org/project/layout_options) module. Just enable the included submodule.


DOCUMENTATION
-------------

Please look into the "example" subfolder for an example GraphQL query.


MAINTAINERS
-----------

* ayalon - https://www.drupal.org/u/ayalon

Supporting organization:

* Liip - https://www.drupal.org/liip
