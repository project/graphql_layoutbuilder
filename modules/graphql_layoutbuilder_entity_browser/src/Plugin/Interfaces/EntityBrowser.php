<?php

namespace Drupal\graphql_layoutbuilder_entity_browser\Plugin\GraphQL\Interfaces;

use Drupal\graphql\Plugin\GraphQL\Interfaces\InterfacePluginBase;

/**
 * @GraphQLInterface(
 *   id = "entity_browser",
 *   name = "EntityBrowser",
 *   type = "entity_browser",
 *   description = @Translation("Entity browser representations.")
 * )
 */
class EntityBrowser extends InterfacePluginBase {

}
