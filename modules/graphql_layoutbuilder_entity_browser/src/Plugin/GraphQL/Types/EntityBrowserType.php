<?php

namespace Drupal\graphql_layoutbuilder_entity_browser\Plugin\GraphQL\Types;

use Drupal\entity_browser_block\Plugin\Block\EntityBrowserBlock;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;
use Drupal\graphql\Utility\StringHelper;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * @GraphQLType(
 *   id = "entity_browser_type",
 *   deriver = "Drupal\graphql_layoutbuilder_entity_browser\Plugin\Deriver\EntityBrowserTypeDeriver",
 * )
 */
class EntityBrowserType extends TypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function applies($object, ResolveContext $context, ResolveInfo $info) {

    if ($object instanceof EntityBrowserBlock) {
      $name = $this->getDefinition()['name'];
      $id = str_replace('entity_browser_block:', '', $object->getConfiguration()['id']);
      $typeName = StringHelper::camelCase('block_entity_browser_' . $id);
      return $typeName === $name;
    }
    return FALSE;
  }

}
