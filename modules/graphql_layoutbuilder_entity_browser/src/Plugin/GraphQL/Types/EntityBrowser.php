<?php

namespace Drupal\graphql_layoutbuilder_entity_browser\Plugin\GraphQL\Types;

use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;

/**
 * @GraphQLType(
 *   id = "entity_browser",
 *   name = "EntityBrowser",
 * )
 */
class EntityBrowser extends TypePluginBase {


}
