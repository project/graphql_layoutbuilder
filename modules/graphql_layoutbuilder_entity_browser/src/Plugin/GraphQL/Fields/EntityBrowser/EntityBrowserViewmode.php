<?php

namespace Drupal\graphql_layoutbuilder_entity_browser\Plugin\GraphQL\Fields\EntityBrowser;

use Drupal\entity_browser_block\Plugin\Block\EntityBrowserBlock;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "entity_browser_viewmode",
 *   secure = true,
 *   name = "entityViewmode",
 *   type = "String",
 *   parents = {"EntityBrowser"}
 * )
 */
class EntityBrowserViewmode extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof EntityBrowserBlock) {

      /** @var \Drupal\entity_browser_block\Plugin\Block\EntityBrowserBlock $block */
      $block = $context->getContext('block', $info);
      $configuration = $block->getConfiguration();

      if (isset($configuration['view_modes']) && is_array($configuration['view_modes'])) {
        $viewmode = reset($configuration['view_modes']);
        yield $viewmode ?? NULL;
      }
    }
  }

}
