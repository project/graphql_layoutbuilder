<?php

namespace Drupal\graphql_layoutbuilder_entity_browser\Plugin\GraphQL\Fields\EntityBrowser;

use Drupal\entity_browser_block\Plugin\Block\EntityBrowserBlock;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "entity_browser_entity",
 *   secure = true,
 *   name = "entity",
 *   type = "Entity",
 *   parents = {"EntityBrowser"},
 * )
 */
class EntityBrowserEntity extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof EntityBrowserBlock) {
      $configuration = $value->getConfiguration();
      $entities = $value->loadEntitiesByIDs($configuration['entity_ids']);
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      $entity = reset($entities);

      if ($entity) {
        yield $entity;
      }
    }
  }

}
