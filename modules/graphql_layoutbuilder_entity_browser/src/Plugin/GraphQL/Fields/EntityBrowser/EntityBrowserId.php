<?php

namespace Drupal\graphql_layoutbuilder_entity_browser\Plugin\GraphQL\Fields\EntityBrowser;

use Drupal\entity_browser_block\Plugin\Block\EntityBrowserBlock;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "entity_browser_id",
 *   secure = true,
 *   name = "id",
 *   type = "String",
 *   parents = {"EntityBrowser"},
 * )
 */
class EntityBrowserId extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof EntityBrowserBlock) {
      yield $value->getConfiguration()['id'];
    }
  }

}
