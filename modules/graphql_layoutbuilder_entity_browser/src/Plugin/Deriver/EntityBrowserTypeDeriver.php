<?php

namespace Drupal\graphql_layoutbuilder_entity_browser\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\graphql\Utility\StringHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EntityBrowserTypeDeriver
 *
 * Deriver for section types.
 *
 * @package Drupal\graphql_layoutbuilder_entity_browser\Plugin\Deriver
 */
class EntityBrowserTypeDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager, LayoutPluginManagerInterface $layout_plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->layoutPluginManager = $layout_plugin_manager;
  }

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.core.layout')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    $entity_browser = $this->entityTypeManager->getStorage('entity_browser')->loadMultiple();
    foreach ($entity_browser as $entityBrowserType => $definition) {

      $type_prefix = 'block_entity_browser_' . $entityBrowserType;

      $derivative = [
        'name' => StringHelper::camelCase($type_prefix),
        'interfaces' => ['Entity', 'EntityBrowser'],
        'type' => $entityBrowserType,
      ] + $basePluginDefinition;
      $this->derivatives[$type_prefix] = $derivative;
    }
    return parent::getDerivativeDefinitions($basePluginDefinition);
  }

}
