<?php

namespace Drupal\graphql_layout_options\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\graphql\Utility\StringHelper;
use Drupal\layout_options\LayoutOptionPluginManager;
use Drupal\layout_options\Plugin\Layout\LayoutOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver for section types.
 */
class LayoutOptionsFieldDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var \Drupal\layout_options\LayoutOptionPluginManager
   */
  protected $layoutOptionPluginManager;

  /**
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  public function __construct(
    LayoutOptionPluginManager $layout_option_plugin_manager,
    LayoutPluginManagerInterface $layout_plugin_manager,
    ModuleHandlerInterface $moduleHandler,
    ThemeHandlerInterface $themeHandler
  ) {
    $this->layoutOptionPluginManager = $layout_option_plugin_manager;
    $this->layoutPluginManager = $layout_plugin_manager;
    $this->moduleHandler = $moduleHandler;
    $this->themeHandler = $themeHandler;
  }

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.layout_options'),
      $container->get('plugin.manager.core.layout'),
      $container->get('module_handler'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    foreach ($this->layoutPluginManager->getDefinitions() as $plugin_id => $definition) {
      if ($definition->getClass() !== 'Drupal\layout_options\Plugin\Layout\LayoutOptions') {
        continue;
      }

      $layout = $this->getLayout($plugin_id, $definition);
      $schema = $layout->getLayoutOptionsSchema();

      if (empty($schema['layout_options'][$plugin_id])) {
        continue;
      }

      $type = 'layout_' . $plugin_id . '_options';
      $derivative = [
        'name' => 'layoutOptions',
        'parents' => [StringHelper::camelCase('layout_' . $plugin_id)],
        // 'interfaces' => ['LayoutOptions'],
        'type' => StringHelper::camelCase($type),
      ] + $basePluginDefinition;
      $this->derivatives[$type] = $derivative;
    }

    return parent::getDerivativeDefinitions($basePluginDefinition);
  }

  public function getLayout($plugin_id, $definition) {
    return new LayoutOptions(
      [],
      $plugin_id,
      $definition,
      $this->moduleHandler,
      $this->themeHandler,
      $this->layoutOptionPluginManager
    );
  }

}
