<?php

namespace Drupal\graphql_layout_options\Plugin\Deriver;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\graphql\Utility\StringHelper;
use Drupal\layout_options\LayoutOptionPluginManager;
use Drupal\layout_options\Plugin\Layout\LayoutOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Deriver for section types.
 */
class LayoutOptionsOptionDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var \Drupal\layout_options\LayoutOptionPluginManager
   */
  protected $layoutOptionPluginManager;

  /**
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutPluginManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme handler service.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  public function __construct(
    LayoutOptionPluginManager $layout_option_plugin_manager,
    LayoutPluginManagerInterface $layout_plugin_manager,
    ModuleHandlerInterface $moduleHandler,
    ThemeHandlerInterface $themeHandler
  ) {
    $this->layoutOptionPluginManager = $layout_option_plugin_manager;
    $this->layoutPluginManager = $layout_plugin_manager;
    $this->moduleHandler = $moduleHandler;
    $this->themeHandler = $themeHandler;
  }

  /**
   * @inheritdoc
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.layout_options'),
      $container->get('plugin.manager.core.layout'),
      $container->get('module_handler'),
      $container->get('theme_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    /** @var \Drupal\Core\Layout\LayoutDefinition[] $definitions */
    $definitions = $this->layoutPluginManager->getDefinitions();

    foreach ($definitions as $plugin_id => $definition) {
      if ($definition->getClass() !== 'Drupal\layout_options\Plugin\Layout\LayoutOptions') {
        continue;
      }

      $layout = $this->getLayout($plugin_id, $definition);
      $schema = $layout->getLayoutOptionsSchema();

      if (empty($schema['layout_options'][$plugin_id])) {
        continue;
      }

      /** @var string[] $options */
      $options = array_keys($schema['layout_options'][$plugin_id]);

      foreach ($options as $option) {
        $options_definition = $schema['layout_option_definitions'][$option];
        $parents = [StringHelper::camelCase('layout_' . $plugin_id . '_options')];

        if (in_array(
          $options_definition['plugin'],
          [
            'layout_options_class_select',
            'layout_options_class_radios',
            'layout_options_class_string',
            'layout_options_id',
          ]
        )) {
          $type = !empty($options_definition['multi']) && $options_definition['multi'] ? '[string]' : 'string';

          $derivative = [
            'id' => $option,
            'name' => StringHelper::propCase($option),
            'description' => $options_definition['description'],
            'parents' => $parents,
            'type' => $type,
          ] + $basePluginDefinition;
          $this->setDerivative($option, $derivative);
        }
        elseif ($options_definition['plugin'] === 'layout_options_class_checkboxes') {
          $checkboxes = array_keys($options_definition['options']);
          foreach ($checkboxes as $checkbox) {
            $name = $option . '____' . $checkbox;
            $derivative = [
              'name' => StringHelper::propCase($name),
              'description' => $options_definition['description'],
              'parents' => $parents,
              'type' => 'boolean',
            ] + $basePluginDefinition;
            $this->setDerivative($name, $derivative);
          }
        }
      }
    }

    return parent::getDerivativeDefinitions($basePluginDefinition);
  }

  public function setDerivative($id, $derivative) {
    if (!empty($this->derivatives[$id])) {
      $this->derivatives[$id]['parents'][] = $derivative['parents'][0];
    }
    else {
      $this->derivatives[$id] = $derivative;
    }
  }

  public function getLayout($plugin_id, $definition) {
    return new LayoutOptions(
      [],
      $plugin_id,
      $definition,
      $this->moduleHandler,
      $this->themeHandler,
      $this->layoutOptionPluginManager
    );
  }

}
