<?php

namespace Drupal\graphql_layout_options\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\layout_builder\Section;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "layout_options_option",
 *   secure = true,
 *   deriver = "Drupal\graphql_layout_options\Plugin\Deriver\LayoutOptionsOptionDeriver",
 * )
 */
class LayoutOptionsOption extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    if ($value instanceof Section) {
      /** @var \Drupal\layout_options\Plugin\Layout\LayoutOptions $layout */
      $layout = $value->getLayout();
      $plugin_id = $this->getPluginId();
      $settings = $value->getLayoutSettings();

      // layout_options_option:option____checkbox => option____checkbox.
      $part = explode(':', $plugin_id)[1];
      $is_checkbox = strpos($part, '____') !== FALSE;

      if ($is_checkbox) {
        // option____checkbox => [option, checkbox].
        list($option, $checkbox) = explode('____', $part);
        if (isset($settings[$option]) && is_array($settings[$option])) {
          yield in_array($checkbox, $settings[$option], TRUE);
        }
      }
      else {
        $schema = $layout->getLayoutOptionsSchema();
        $definition = $schema['layout_option_definitions'][$part];

        if (!empty($definition['multi']) && $definition['multi']) {
          $selected = array_filter($settings[$part], 'is_int', ARRAY_FILTER_USE_KEY);
          foreach (array_unique($selected) as $item) {
            yield $item;
          }
        }
        else {
          yield is_string($settings[$part]) ? $settings[$part] : NULL;
        }
      }
    }
  }

}
