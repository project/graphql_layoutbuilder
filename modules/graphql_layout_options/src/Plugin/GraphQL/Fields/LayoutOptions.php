<?php

namespace Drupal\graphql_layout_options\Plugin\GraphQL\Fields;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 *
 * @GraphQLField(
 *   id = "layout_options_field",
 *   secure = true,
 *   deriver = "Drupal\graphql_layout_options\Plugin\Deriver\LayoutOptionsFieldDeriver",
 * )
 */
class LayoutOptions extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    yield $value;
  }

}
