<?php

namespace Drupal\graphql_layout_options\Plugin\GraphQL\Types;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use GraphQL\Type\Definition\ResolveInfo;
use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;
use Drupal\layout_builder\Section;

/**
 * @GraphQLType(
 *   id = "layout_option_type",
 *   name = "LayoutOptions",
 *   deriver = "Drupal\graphql_layout_options\Plugin\Deriver\LayoutOptionTypeDeriver",
 * )
 */
class LayoutOptionType extends TypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function applies($object, ResolveContext $context, ResolveInfo $info) {
    if ($object instanceof Section) {
      $type = $this->getPluginDefinition()['type'];
      $section_type = 'layout_' . $object->getLayoutId() . '_options';
      return $type === $section_type;
    }

    return FALSE;
  }

}
